<?php

if (!function_exists('d')) {
    function d($data)
    {
        echo "<pre>";
        var_dump($data);
    }
 }
 if (!function_exists('dd')) {
    function dd($data)
    {
        echo "<pre>";
        var_dump($data);
        die;
    }
 }