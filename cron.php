<?php
require 'vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'CurlService.php';
include("simple_html_dom.php");
require 'dump.php';

if(file_exists('emails.json')){
      $string = file_get_contents("emails.json",true);
      if(empty($string)){
          return;
      }
      $arrayEmail = json_decode($string);
      foreach($arrayEmail as $item){
          $emailKeyword = file_get_contents($item.'.json',true);
          if (!empty($emailKeyword)) {

            $keywords = json_decode($emailKeyword);
            $html = [];
            foreach ($keywords as $key) {
                $result = searchKeyword($key);

                $html[] = 'Keyword: ' . $key;
                $html[] = 'Results: ' .$result['count'];
                $html[] = 'http://pinterest.hoanvusolutions.com.vn/#' . $key;
                $html[] = '';

                if(count($result['results'])) {
                    $limitShow = 10;
                    foreach ($result['results'] as $key=>$row) {
                        if($key >= $limitShow) break;

                        $html[]='('.($key+1).')';
                        $html[]= '<a href="'.$row['link'].'">'.$row['title'].'</a>';
                        $html[]= $row['desc'];
                        $html[]= '';
                    }
                }
                $html[] = '';
            }
            $html[] = "If  you don't want to receive email alerts anymore, please click <a href='http://pinterest.hoanvusolutions.com.vn/?email=".$item."'>Here</a> to cancel.";
            $htmlEle = join('<br>',$html);
            sendMail($item, 'Subject', $htmlEle);
        }
    }

}

function sendMail($email,$subject,$body){
      $mail = new PHPMailer();                              // Passing `true` enables exceptions
        try {

            //Server settings
          //  $mail->SMTPDebug = 2;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'spcenter2018@gmail.com';                 // SMTP username
            $mail->Password = 'qhhkvhqcqtakxlol';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to
            //Recipients
            $mail->setFrom('from@example.com', 'Mailer');
             // Name is optional
             $mail->addAddress($email);
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $body;

            $mail->send();
            echo 'Message has been sent '.$email.'';
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
}
function searchKeyword($keyword) {
    $arrResult = [];
    $resultCount = 20;

    $arrSearchEngines = [];
    $arrSearchEngines[] = 'https://www.google.com/search?hl=en&tbo=d&site=&source=hp&q='; //for google
    $arrSearchEngines[] = 'http://www.bing.com/search?hl=en&tbo=d&site=&source=hp&q='; // for bing
    $arrSearchEngines[] = 'https://www.hotbot.com/web?q='; //for hotbot
    $arrSearchEngines[] = 'https://www.googleapis.com/customsearch/v1?key='; // for google custom search
    $arrSearchEngines[] = 'https://uk.search.yahoo.com/search?p='; // For yahoo

    //-------CAN BE CHANGED. PLEASE APPEND THE + SIGN AT THE END OF ANY URL - Google search engine limits the queries to 32 words

    $sites = [
        'site:http://answers.com',
        'site:http://blogspot.co.uk',
        'site:http://digg.com',
        'site:http://foursquare.com',
        'site:http://reddit.com',
        'site:http://www.avvo.com',
        'site:http://www.bbb.org',
        'site:http://www.consumerreports.org',
        'site:http://www.glassdoor.com/Reviews',
        'site:http://www.homeadvisor.com',
        'site:http://www.testfreaks.com',
        'site:http://www.tripadvisor.com',
        'site:http://www.which.co.uk',
        'site:https://del.icio.us',
        'site:https://flipboard.com',
        'site:https://kudzu.com',
        'site:https://opentable.com',
        'site:https://quora.com',
        'site:https://ratefate.com',
        'site:https://trustpilot.com/reviews',
        'site:https://tumblr.com',
        'site:https://twitter.com/search',
        'site:https://www.angieslist.com',
        'site:https://www.facebook.com/search',
        'site:https://www.g2crowd.com',
        'site:https://www.manta.com',
        'site:https://www.trustradius.com',
        'site:https://www.yelp.com',
    ];

    foreach ($sites as $key => $site) {
        if ($key == 0){
            $query = $site;
        } else {
            $query .= '+OR+' . $site;
        }
    }
    // $query = 'site:http://answers.com+'; // Can be changed anytime as per requirement
    // $query='';
    /**
     * For Google
     */
    $query = str_replace('/', '%2F', $query);
    $query = str_replace(':', '%3A', $query);

    $curlService = new CurlService();
    $url = $arrSearchEngines[0] . $keyword .'+' . $query . $keyword . '&num=' . $resultCount . '';
    // $googleResult = false;
    $googleResult = $curlService->google($url);

    //if google block
    if($googleResult === false) {
        //search by bing
        $url = $arrSearchEngines[1] . $keyword .'+' . $query . '&count=' . $resultCount . '';
        $bingResult = $curlService->bing($url);
        if($bingResult === false) {
            //Use hotbot to search
            $url = $arrSearchEngines[2] . $keyword .'+' . $query;
            $hotbotResult = $curlService->hotbot($url);
            if($hotbotResult === false) {
                //Use yahoo to search
                $url = $arrSearchEngines[4] . $keyword .'+' . $query.'&n='. $resultCount. '';
                $yahooResult = $curlService->yahoo($url);
                if($yahooResult === false) {
                    $error = 'Your daily limit has been reached! Try again tomorrow';
                } else {
                    $arrResult = $yahooResult;
                    $searchEngine = 'Yahoo';
                }
            } else {
                //Hotbot result not empty
                $arrResult = $hotbotResult;
                $searchEngine = 'Hotbot';
            }
        } else {
            //Bing result not empty
            $arrResult = $bingResult;
            $searchEngine = 'Bing';
        }
    } else {
        //if google result not empty
        $arrResult = $googleResult;
        $searchEngine = 'Google';
    }

    return $arrResult;
}

?>