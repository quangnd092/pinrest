<!--==================== Login Home ==================== -->

        <div class="modal fade" id="LoginHome">
          <div class="modal-dialog">
            <div style="background-color: #fff;" class="modal-content text-center">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"><b>Login with your social account get started</b></h3>
				<p>Please accept all app permissions to use all features.</p>
              </div>
              <div class="modal-body">
			  
              <a href="https://suite.social/dashboard/login/facebook?ref=tools" target="_top" class="btn btn-lg btn-social btn-facebook">
                <i class="fa fa-facebook"></i> Sign in with Facebook
              </a>
              <a href="https://suite.social/dashboard/login/youtube?ref=tools" target="_top" class="btn btn-lg btn-social btn-youtube">
                <i class="fa fa-youtube-play"></i> Sign in with YouTube
              </a>
              <a href="https://suite.social/dashboard/login/linkedin?ref=tools" target="_top" class="btn btn-lg btn-social btn-linkedin">
                <i class="fa fa-linkedin"></i> Sign in with LinkedIn
              </a>
              <a href="https://suite.social/dashboard/login/twitter?ref=tools" target="_top" class="btn btn-lg btn-social btn-twitter">
                <i class="fa fa-twitter"></i> Sign in with Twitter
              </a>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success pull-right" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		
<!--==================== Login Tools ==================== --> 
  
        <div class="modal fade" id="LoginTools">
          <div class="modal-dialog">
            <div style="background-color: #fff;" class="modal-content text-center">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"><b>Login with your social account get started</b></h3>
				<p>Please accept all app permissions to use all features.</p>
              </div>
              <div class="modal-body">
			  
              <a href="https://suite.social/dashboard/login/facebook?ref=tools" target="_top" class="btn btn-lg btn-social btn-facebook">
                <i class="fa fa-facebook"></i> Sign in with Facebook
              </a>
              <a href="https://suite.social/dashboard/login/youtube?ref=tools" target="_top" class="btn btn-lg btn-social btn-youtube">
                <i class="fa fa-youtube-play"></i> Sign in with YouTube
              </a>
              <a href="https://suite.social/dashboard/login/linkedin?ref=tools" target="_top" class="btn btn-lg btn-social btn-linkedin">
                <i class="fa fa-linkedin"></i> Sign in with LinkedIn
              </a>
              <a href="https://suite.social/dashboard/login/twitter?ref=tools" target="_top" class="btn btn-lg btn-social btn-twitter">
                <i class="fa fa-twitter"></i> Sign in with Twitter
              </a>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success pull-right" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		
<!--==================== SEO ==================== --> 
        <div class="modal fade" id="SEO">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"><b>Social SEO</b> <span class="text-muted">- Get found everywhere with Local Listing Service</span></h3>
              </div>
              <div class="modal-body">
			  
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1seo" data-toggle="tab"><b>1.</b> Local Listings <i class="fa fa-arrow-circle-right"></i></a></li>
              <li><a href="#tab_2seo" data-toggle="tab"><b>2.</b> Business Monitoring <i class="fa fa-arrow-circle-right"></i></a></li>
			  <li><a href="#tab_3seo" data-toggle="tab"><b>3.</b> Review Tracking <i class="fa fa-arrow-circle-right"></i></a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1seo">
			  
<div class="row">
                <div style="background: #fff;" class="box box-default">
                    <div class="box-body">

                        <div class="col-md-6">
						<br>	
						<img width="100%" src="//suite.social/images/screen/seo-listings.png" alt="Listings">	
						<p><a onClick="popup('https://www.local-marketing-reports.com/location-dashboard/32cfae3c73039e4506f2491b0d88f512b85b073d/citation-reports#ct-top')" class="btn btn-success btn-lg btn-block"><i class="fa fa-file-text"></i> VIEW SAMPLE REPORT</a></p>	
						<p><a href="#UpgradeSEO" data-toggle="modal" data-dismiss="modal" class="btn btn-primary btn-lg btn-block"><i class="fa fa-credit-card"></i> SEE PRICING</a></p>
						
                        </div>

                        <div class="col-md-6">		
						

<h4><b><i class="fa fa-send"></i> Submission</b></h4>

<p>Be where your customers are searching! We have a dedicated team that can regularly submit your business data to thousands of online local business directories worldwide such as mobile apps, mapping services, search engines, GPS devices and more per month.</p>
<ul>
<li>Get new, accurate listings</li>
<li>Data Aggregator Submissions</li>
<li>Get listed on Google My Business + Bing local + Yahoo Local</li>
<li>Get help removing harmful duplicate listings</li>
<li>Directory clean-up (fix address errors)</li>
</ul>

<hr>

<h4><b><i class="fa fa-graduation-cap"></i> Authority</b></h4>
<p>Get business listings on powerful sites that boost your authority. We submit to important local directories including:</p>
<ul>
<li>Google My Business</li>
<li>Bing Local</li>
<li>Yelp</li>
<li>Facebook</li>
<li>Foursquare</li>
<li>Apple Maps</li>
<li>YellowPages</li>
<li>TripAdvisor</li>
</ul>

<hr>

<h4><b><i class="fa fa-file-text"></i> Reports</b></h4>
Get monthly professional report by email with all local listing submissions and status.
						
                        </div>

                    </div>
                </div>
						
        </div>			
						
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2seo">

<div class="row">
                <div style="background: #fff;" class="box box-default">
                    <div class="box-body">

                        <div class="col-md-6">
						<br>	
						<img width="100%" src="//suite.social/images/screen/seo-monitoring.png" alt="Monitoring">						
                        </div>

                        <div class="col-md-6">		

<h4><b><i class="fa fa-user"></i> Ownership</b></h4>
We can find, fix and monitor your local business listings daily across the entire web. Once we claim and verify your business listings, you own them forever. 

<hr>

<h4><b><i class="fa fa-map-o"></i> Updates</b></h4>
Free data updates and refresh for 12 months. So if you move location, switch phone number or change your working hours, then don’t worry. We can update your business information free for 12 months after initial submission.

<hr>

<h4><b><i class="fa fa-file-text"></i> Reports</b></h4>
Get monthly professional report by email with all your local listings, reviews and stats.

						<p><a onClick="popup('https://www.local-marketing-reports.com/location-dashboard/32cfae3c73039e4506f2491b0d88f512b85b073d/citation-reports#ct-top')" class="btn btn-success btn-lg btn-block"><i class="fa fa-file-text"></i> VIEW SAMPLE REPORT</a></p>	
						<p><a href="#UpgradeSEO" data-toggle="modal" data-dismiss="modal" class="btn btn-primary btn-lg btn-block"><i class="fa fa-credit-card"></i> SEE PRICING</a></p>
						
                        </div>

                    </div>
                </div>
						
        </div>			  
			  
              </div>
              <!-- /.tab-pane -->
			  
              <div class="tab-pane" id="tab_3seo">

<div class="row">
                <div style="background: #fff;" class="box box-default">
                    <div class="box-body">

                        <div class="col-md-6">
						<br>	
						<img width="100%" src="//suite.social/images/screen/seo-reviews.png" alt="Reviews">	
					
                        </div>

                        <div class="col-md-6">		

<h4><b><i class="fa fa-comments"></i> Tracking</b></h4>
We can track your online reviews and ratings on popular review sites like Yelp, Google Local, Foursquare, YP.com, CitySearch, and Superpages and don’t let negative comments spoil your reputation.

<hr>

<h4><b><i class="fa fa-envelope"></i> Alerts</b></h4>
Get alerted when you have a new review so you don't get caught out by negative reviews. We run regular sweeps of important review sites to identify any new reviews you have. We email you straight away so you can quickly act to fix the negative ones, or thank your loyal customers for their positive comments.

<hr>

<h4><b><i class="fa fa-file-text"></i> Reports</b></h4>
Get monthly professional report by email with all your reviews and ratings.

						<p><a onClick="popup('https://www.local-marketing-reports.com/location-dashboard/32cfae3c73039e4506f2491b0d88f512b85b073d/citation-reports#ct-top')" class="btn btn-success btn-lg btn-block"><i class="fa fa-file-text"></i> VIEW SAMPLE REPORT</a></p>	
						<p><a href="#UpgradeSEO" data-toggle="modal" data-dismiss="modal" class="btn btn-primary btn-lg btn-block"><i class="fa fa-credit-card"></i> SEE PRICING</a></p>
						
                        </div>

                    </div>
                </div>
						
        </div>			  
			  
              </div>
              <!-- /.tab-pane -->	
			  
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->			  			  
			  
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success pull-right" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		
<!--==================== SEO PRICES ==================== --> 

<div class="modal fade" id="UpgradeSEO">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Order Social SEO</h4>
            </div>
            <div class="modal-body">
					
<div class="row">
                <div class="box box-default">
                    <div class="box-body">
          
        <!-- Col 1 -->  
        <div class="col-md-4">
          <div class="box box-widget widget-user-2">
            <!-- Header -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="//suite.social/images/favicon/apple-touch-icon-114x114.png" alt="Icon">
              </div>
              <h3 class="widget-user-username"><b>3 Months Service</b></h3>
              <h4 class="widget-user-desc">€499/mo</h4>
            </div>
            <!-- Features -->
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">				
                <li><a href="#">Local Listings (1-5 locations) <span class="pull-right fa fa-check"></span></a></li>
                <li><a href="#">Business Monitoring <span class="pull-right fa fa-check"></span></a></li>
                <li><a href="#">Review Tracking <span class="pull-right fa fa-check"></span></a></li>
                <li><a href="#">Monthly Reports <span class="pull-right fa fa-check"></span></a></li>			
              </ul>
            </div>
          </div>
          <a style="color:#fff" href="#" onClick="popupSmall('https://m.me/www.suite.social')" class="btn btn-primary btn-block"><i class="fa fa-arrow-right"></i> ORDER NOW!</a>
        </div>
        <!-- / Col 1 -->
        
        <!-- Col 2 -->      
        <div class="col-md-4">
          <div class="box box-widget widget-user-2">
            <!-- Header -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="//suite.social/images/favicon/apple-touch-icon-114x114.png" alt="Icon">
              </div>
              <h3 class="widget-user-username"><b>6 Months Service</b></h3>
              <h4 class="widget-user-desc">€449/mo</h4>
            </div>
            <!-- Features -->
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="#">Local Listings (1-5 locations) <span class="pull-right fa fa-check"></span></a></li>
                <li><a href="#">Business Monitoring <span class="pull-right fa fa-check"></span></a></li>
                <li><a href="#">Review Tracking <span class="pull-right fa fa-check"></span></a></li>
                <li><a href="#">Monthly Reports <span class="pull-right fa fa-check"></span></a></li>
              </ul>
            </div>
          </div>
          <a style="color:#fff" href="#" onClick="popupSmall('https://m.me/www.suite.social')" class="btn btn-primary btn-block"><i class="fa fa-arrow-right"></i> ORDER NOW!</a>
        </div>
        <!-- / Col 2 -->
        
        <!-- Col 3 -->      
        <div class="col-md-4">
          <div class="box box-widget widget-user-2">
            <!-- Header -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="//suite.social/images/favicon/apple-touch-icon-114x114.png" alt="Icon">
              </div>
              <h3 class="widget-user-username"><b>12 Months Service</b></h3>
              <h4 class="widget-user-desc">€399/mo</h4>
            </div>
            <!-- Features -->
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="#">Local Listings (1-5 locations) <span class="pull-right fa fa-check"></span></a></li>
                <li><a href="#">Business Monitoring <span class="pull-right fa fa-check"></span></a></li>
                <li><a href="#">Review Tracking <span class="pull-right fa fa-check"></span></a></li>
                <li><a href="#">Monthly Reports <span class="pull-right fa fa-check"></span></a></li>
              </ul>
            </div>
          </div>
          <a style="color:#fff" href="#" onClick="popupSmall('https://m.me/www.suite.social')" class="btn btn-primary btn-block"><i class="fa fa-arrow-right"></i> ORDER NOW!</a>
        </div>
        <!-- / Col 3 -->      

                    </div>
                </div>
						
        </div>			
            
            </div>            
            <div class="modal-footer">
                <button type="button" class="btn btn-success pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 

<!------------------------------ Footer ------------------------------>

				
 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 10.0
    </div>
    <strong>&copy; <?php echo date('Y'); ?> - <a href="//suite.social">Social Suite.</a></strong> All Rights Reserved. <a href="//suite.social/contact">Contact</a> I <a href="//suite.social/terms">Terms & Conditions</a> I <a href="//suite.social/privacy">Privacy Policy</a>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">	  
	  
        CONTENT HERE		
				
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- Exit popup -->
<script src="//suite.social/src/js/exit.js"></script>
<!-- Date Picker -->
<script src="//suite.social/src/js/jquery.simple-dtpicker.js"></script>
<!-- Notifications jQuery plugin -->
<script type="text/javascript" src="//suite.social/src/js/jquery.notification.min.js"></script>
<!-- Spintax -->
<script src="//suite.social/src/js/spintax.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="//suite.social/src/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="//suite.social/src/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="//suite.social/src/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="//suite.social/src/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Toggle -->
<script src="//suite.social/src/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>
<!-- SlimScroll -->
<script src="//suite.social/src/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="//suite.social/src/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="//suite.social/src/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes
<script src="//suite.social/src/dist/js/demo.js"></script> -->
<!-- page script -->

<script>
// CALCULATION

$('.hamb-wrap').on('click', function(){
  $(this).parent().children('p').toggle();
  $(this).children().toggleClass('active');
  $('nav').fadeToggle(200);
})
  </script>
  
	<script>
		$(document).ready(function() {
			$('#notification-1').Notification({
				// Notification varibles
				Varible1: ["Adeel","Ajay","Alana","Alex","Alexander","Alfie","Alice","Alina","Allen","Aman","Amber","Ammad","Amy","Anna","Anne","Antonia","Arielle","Ashley","Betty","Caezar","Carmel","Casey","Charlie","Chloe","Chris","Christine","Clara","Cleo","Colin","Courtney","Dan","Daniel","Dave","Debby","Deepika","Elise","Ellie","Emily","Emma","Gina","Hana","Heather","Holly","Jack","Jade","Jamar","Jamie","Jana","Jasdeep","Jasmine","Javed","Javier","Jay","Jaylana","Jelena","Jenna","Jennifer","Jeon","Jess","Jessica","Jessie","Joanna","Jodie","Joe","John","Jolene","Juang","Justine","Kamran","Kate","Katie","Katy","Kelsey","Kenny","Keri","Kerry","Kevin","Kim","Kris","Lavina","Layla","Lee","Lexi","Lilly","Lisa","Lola","Lucy","Lukasz","Mark","Matt","Max","Melanie","Mia","Michelle","Molly","Monica","Muhammad","Natalie","Nellie","Nick","Olivia","Pardeep","Paul","Paula","Polly","Quinn","Rachael","Raju","Rashid","Ray","Robin","Rosales","Sally","Sam","Samantha","Sanket","Sara","Silvana","Steve","Suman","Susan","Tammy","Tasha","Timmy","Tom","Tony","Valerie","Victoria","Viktor","Vlad","Yasmine","Yura","Zoe"], // ADD NAMES
				Varible2: ["started a free trial!","used a free social media tool!","completed free social media training!","subscribed to Social Suite Pro!"],
				Amount: [1000, 25000], // THIS IS PRICES, NOT NEEDED
				Content: '<b>[Varible1]</b><br> Has just [Varible2]<br><b><a style="color:#609450" href="#"><u>TRY YOURSELF</u></a></b>',
				// Timer
				Show: ['random', 5, 60], // SHOWS RANDOM FROM 3 - 60 SECONDS
				Close: 7,
				// Notification style 
				LocationTop: [false, '5%'],
				LocationBottom:[true, '2%'],
				LocationRight: [false, '20px'],						
				LocationLeft:[true, '10px'],
				Background: 'white',
				BorderRadius: 5,
				BorderWidth: 1,
				BorderColor: '#609450',
				TextColor: '#1f1f1f',
				IconColor: 'gold',
				// Notification Animated   
				AnimationEffectOpen: 'slideInUp',
				AnimationEffectClose: 'slideOutDown',
				// Number of notifications
				Number: 99, //NUMBER OF NOTFICATIONS
				// Notification link
				Link: [true, 'https://suite.social/dashboard/members', '_blank']				
			});			
			
		});				
		
	</script>

<script type="text/javascript">
				
  function popup(url)
  {
  var w = 1250;
  var h = 700;
  var title = 'Social Tool';
  var left = (screen.width / 2) - (w / 2);
  var top = (screen.height / 2) - (h / 2);
  window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
  }
  
  function popupSmall(url)
  {
  var w = 800;
  var h = 600;
  var title = 'Social Tool';
  var left = (screen.width / 2) - (w / 2);
  var top = (screen.height / 2) - (h / 2);
  window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
  }
					
</script> 

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
  
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

	$('.modal').on('show.bs.modal', function () {
		$('.modal').not($(this)).each(function () {
			$(this).modal('hide');
		});
	});	 
  
</script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );

$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
});

var activeTab = localStorage.getItem('activeTab');

console.log(activeTab);

if (activeTab) {
   $('a[href="' + activeTab + '"]').tab('show');
}
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-24390400-7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-24390400-7');
</script>


</body>
</html>