<?php

Class CurlService {

    public function get($url, $post_paramtrs=false)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        if($post_paramtrs)
        {
            curl_setopt($c, CURLOPT_POST,TRUE);
            curl_setopt($c, CURLOPT_POSTFIELDS, "var1=bla&".$post_paramtrs );
        }
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0");
        curl_setopt($c, CURLOPT_COOKIE, 'CookieName1=Value;');
        curl_setopt($c, CURLOPT_MAXREDIRS, 10);
        $follow_allowed= ( ini_get('open_basedir') || ini_get('safe_mode')) ? false:true;
        if ($follow_allowed)
        {
            curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
        }
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 9);
        curl_setopt($c, CURLOPT_REFERER, $url);
        curl_setopt($c, CURLOPT_TIMEOUT, 60);
        curl_setopt($c, CURLOPT_AUTOREFERER, true);
        curl_setopt($c, CURLOPT_ENCODING, 'gzip,deflate');
        $data=curl_exec($c);
        $status=curl_getinfo($c);
        curl_close($c);
        preg_match('/(http(|s)):\/\/(.*?)\/(.*\/|)/si',  $status['url'],$link); $data=preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/|\/)).*?)(\'|\")/si','$1=$2'.$link[0].'$3$4$5', $data);   $data=preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/)).*?)(\'|\")/si','$1=$2'.$link[1].'://'.$link[3].'$3$4$5', $data);
        if($status['http_code']==200)
        {
            return $data;
        }
        elseif($status['http_code']==301 || $status['http_code']==302)
        {
            if (!$follow_allowed)
            {
                if (!empty($status['redirect_url']))
                {
                    $redirURL=$status['redirect_url'];
                }
                else
                {
                    preg_match('/href\=\"(.*?)\"/si',$data,$m);
                    if (!empty($m[1]))
                    {
                        $redirURL=$m[1];
                    }
                }
                if(!empty($redirURL))
                {
                    return  call_user_func( __FUNCTION__, $redirURL, $post_paramtrs);
                }
            }
        }
        return "ERRORCODE22 with $url!!<br/>Last status codes<b/>:".json_encode($status)."<br/><br/>Last data got<br/>:$data";
    }

    public function google($url)
    {
        $getData = $this->get($url);
        $html = str_get_html($getData);

        $totalResultElement = $html->find('#resultStats', 0);

        // if block
        if(empty($totalResultElement)) {
            return false;
        }
        //if do not block
        $countText = $totalResultElement->plaintext;
        $regex = "/About\s([\d\,\.]*)\s.*/i";
        $regexFormatNumber = "/[\,\.]/i";
        $check = preg_match($regex, $countText, $matches);

        $count = 0;
        $results = [];

        if(!empty($matches)) {
            $count = isset($matches[1]) ? ((float)preg_replace($regexFormatNumber, '', $matches[1])) : 0;
        }
        //Get list
        $searchResultBoxs = $html->find('.srg .g .rc');

        foreach ($searchResultBoxs as $box) {
            //Get title and href
            $titleBox = $box->find('.r a',0);
            $titleElement = $titleBox->find('h3',0);

            $href = $titleBox->href;
            $title = $titleElement->plaintext;

            //Get description
            $descriptionBox = $box->find('.s .st',0);
            $description = $descriptionBox->innertext;

            //Push title, href, description to result array
            $results[] = [
                'title' => $title,
                'desc' => $description,
                'link' => $href

            ];
        }

        return ['count' => $count, 'results' => $results];
    }

    public function bing($url)
    {
        $getData = $this->get($url);
        $html = str_get_html($getData);
        $totalResultElement = $html->find('.sb_count', 0);

        // dd($totalResultElement);
        // if block
        if(empty($totalResultElement)) {
            return false;
        }

        //if do not block
        $countText = $totalResultElement->innertext;

        $regex = "/([\d\,\.]*)\s.*/i";
        $regexFormatNumber = "/[\,\.]/i";
        $check = preg_match($regex, $countText, $matches);
        // dd($matches);
        $count = 0;
        $results = [];

        if(!empty($matches)) {
            $count = isset($matches[1]) ? ((float)preg_replace($regexFormatNumber, '', $matches[1])) : 0;
        }

        //Get list
        $searchResultBoxs = $html->find('#b_results .b_algo');
        // dd($searchResultBoxs);

        foreach ($searchResultBoxs as $box) {
            //Get title and href
            $titleElement = $box->find('h2 a',0);
            // dd($titleElement);
            $href = $titleElement->href;
            $title = $titleElement->plaintext;

            //Get description
            $descriptionBox = $box->find('.b_caption p', 0);
            $description = $descriptionBox->innertext;

            //Push title, href, description to result array
            $results[] = [
                'title' => $title,
                'desc' => $description,
                'link' => $href
            ];
        }
        return ['count' => $count, 'results' => $results];
    }

    public function hotbot($url)
    {
        return false;
    }

    public function yahoo($url)
    {
        $getData = $this->get($url);
        $html = str_get_html($getData);
        $totalResultElement = $html->find('.compPagination span', 0);

        // if block
        if(empty($totalResultElement)) {
            return false;
        }

        //if do not block
        $countText = $totalResultElement->innertext;

        $regex = "/([\d\,\.]*)\s.*/i";
        $regexFormatNumber = "/[\,\.]/i";
        $check = preg_match($regex, $countText, $matches);
        // dd($matches);
        $count = 0;
        $results = [];

        if(!empty($matches)) {
            $count = isset($matches[1]) ? ((float)preg_replace($regexFormatNumber, '', $matches[1])) : 0;
        }

        //Get list
        $searchResultBoxs = $html->find('#main .reg li');

        foreach ($searchResultBoxs as $box) {
            //Get title and href
            $titleElement = $box->find('.dd .compTitle .title a',0);
            if(empty($titleElement)) continue;

            $href = $titleElement->href;
            $title = $titleElement->plaintext;

            //Get description
            $descriptionBox = $box->find('.compText p', 0);
            $findDesAds = $descriptionBox->find('a',0);
            $description = !empty($findDesAds) ? $findDesAds->innertext :  $descriptionBox->innertext;

            //Push title, href, description to result array
            $results[] = [
                'title' => $title,
                'desc' => $description,
                'link' => $href
            ];
        }
        return ['count' => $count, 'results' => [$results[0]]];
    }
}