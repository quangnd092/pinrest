<?php
//******************** SEARCH TOOL ********************//
require 'vendor/autoload.php';
require 'CurlService.php';
require 'dump.php';
include("simple_html_dom.php");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
$error = '';
$resultCount = 10;
$arrResult = $arrSearchEngines = $linkObjs = $sites = array();
//
$arrSearchEngines[] = 'https://www.google.com/search?hl=en&tbo=d&site=&source=hp&q='; //for google
$arrSearchEngines[] = 'http://www.bing.com/search?hl=en&tbo=d&site=&source=hp&q='; // for bing
$arrSearchEngines[] = 'https://www.hotbot.com/web?q='; //for hotbot
$arrSearchEngines[] = 'https://www.googleapis.com/customsearch/v1?key='; // for google custom search
$arrSearchEngines[] = 'https://uk.search.yahoo.com/search?p='; // For yahoo
$googleKey = 'AIzaSyAA83xV_aoE86PG0eg07_mAco2EdBlexH4';
$googleClientId = '008703924232906166783:iau5rhsux34'; // Client ID for google custom search

$i = $j = 0;
$isCustomSearch = 1;
$typeDesc = $descr = '';

if (isset($_GET['email']) && !empty($_GET['email'])) {
    if (file_exists('emails.json')) {
        //chmod("emails.json", 755);
        $string = file_get_contents("emails.json", true);
        if (empty($string)) {
            return;
        }

        $arrayEmail = json_decode($string);
        $index =  array_search ($_GET['email'],$arrayEmail);

        if(isset($arrEmail[$index])){
        unset($arrayEmail[$index]);
            $fp = fopen("emails.json", "w");
            fwrite($fp, json_encode($arrayEmail));
            fclose($fp);
        }
    }
}
if (!empty($_POST)) {
    if(isset($_POST['form'])){

        if (!empty($_POST['keyword'])) {
            $resultCount = isset($_POST['resultsize']) && !empty($_POST['resultsize']) ? $_POST['resultsize'] : $resultCount; // setting default value as 1;
            $keyword = $_POST['keyword'];
            $keyword = str_replace(' ', '+', $keyword); // replace space with +
            //-------CAN BE CHANGED. PLEASE APPEND THE + SIGN AT THE END OF ANY URL - Google search engine limits the queries to 32 words
            $query = '';
            $sites = [
                'site:http://answers.com',
                'site:http://blogspot.co.uk',
                'site:http://digg.com',
                'site:http://foursquare.com',
                'site:http://reddit.com',
                'site:http://www.avvo.com',
                'site:http://www.bbb.org',
                'site:http://www.consumerreports.org',
                'site:http://www.glassdoor.com/Reviews',
                'site:http://www.homeadvisor.com',
                'site:http://www.testfreaks.com',
                'site:http://www.tripadvisor.com',
                'site:http://www.which.co.uk',
                'site:https://del.icio.us',
                'site:https://flipboard.com',
                'site:https://kudzu.com',
                'site:https://opentable.com',
                'site:https://quora.com',
                'site:https://ratefate.com',
                'site:https://trustpilot.com/reviews',
                'site:https://tumblr.com',
                'site:https://twitter.com/search',
                'site:https://www.angieslist.com',
                'site:https://www.facebook.com/search',
                'site:https://www.g2crowd.com',
                'site:https://www.manta.com',
                'site:https://www.trustradius.com',
                'site:https://www.yelp.com',
            ];

            foreach ($sites as $key => $site) {
                if ($key == 0){
                    $query = $site;
                } else {
                    $query .= '+OR+' . $site;
                }
            }
            // $query = 'site:http://answers.com+'; // Can be changed anytime as per requirement
            // $query='';
            /**
             * For Google
             */
            $query = str_replace('/', '%2F', $query);
            $query = str_replace(':', '%3A', $query);

            $curlService = new CurlService();
            $url = $arrSearchEngines[0] . $keyword .'+' . $query . $keyword . '&num=' . $resultCount . '';
            // $googleResult = false;
            $googleResult = $curlService->google($url);

            //if google block
            if($googleResult === false) {
                //search by bing
                $url = $arrSearchEngines[1] . $keyword .'+' . $query . '&count=' . $resultCount . '';
                $bingResult = $curlService->bing($url);
                if($bingResult === false) {
                    //Use hotbot to search
                    $url = $arrSearchEngines[2] . $keyword .'+' . $query;
                    $hotbotResult = $curlService->hotbot($url);
                    if($hotbotResult === false) {
                        //Use yahoo to search
                        $url = $arrSearchEngines[4] . $keyword .'+' . $query.'&n='. $resultCount. '';
                        $yahooResult = $curlService->yahoo($url);
                        if($yahooResult === false) {
                            $error = 'Your daily limit has been reached! Try again tomorrow';
                        } else {
                            $arrResult = $yahooResult['results'];
                            $searchEngine = 'Yahoo';
                        }
                    } else {
                        //Hotbot result not empty
                        $arrResult = $hotbotResult['results'];
                        $searchEngine = 'Hotbot';
                    }
                } else {
                    //Bing result not empty
                    $arrResult = $bingResult['results'];
                    $searchEngine = 'Bing';
                }
            } else {
                //if google result not empty
                $arrResult = $googleResult['results'];
                $searchEngine = 'Google';
            }
           // die;
            //--------------------------------------------------------------------------------------------------------------------------------//

            // if ($html) {
            //     // For Google
            //     $typeDesc = 'google';
            //     $linkObjs = $html->find('h3.r a');
            // } else {
            //     /**
            //      * For BING
            //      */
            //     $q = str_replace('/', '%2F', $q);
            //     $q = str_replace(':', '%3A', $q);
            //     $url = $arrSearchEngines[1] . $q . $forBing . '&count=' . $resultCount . '';

            //     $html = file_get_html($url);

            //     if ($html) {
            //         // For Bing
            //         $typeDesc = 'bing';
            //         $linkObjs = $html->find('h2 a');
            //     } else {
            //         /**
            //          * For Hotbot
            //          */
            //         $q = str_replace('/', '%2F', $q);
            //         $q = str_replace(':', '%3A', $q);
            //         $url = $arrSearchEngines[2] . $q . $in . '';

            //         $html = file_get_html($url);

            //         if ($html) {
            //             // For Hotbot
            //             $typeDesc = 'hotbot';
            //             $linkObjs = $html->find('h3 a');
            //         } else {
            //             /**
            //              * For Yahoo
            //              */
            //             $q = str_replace('/', '%2F', $q);
            //             $q = str_replace(':', '%3A', $q);
            //             $url = $arrSearchEngines[4] . $q . $in . '&pz=' . $resultCount . '';
            //             $html = file_get_html($url);

            //             if ($html) {
            //                 //For yahoo
            //                 $typeDesc = 'yahoo';
            //                 $linkObjs = $html->find('h3 a');
            //             } else {
            //                 //Google Custom Search
            //                 $isCustomSearch = 2;
            //                 $url = $arrSearchEngines[3] . $googleKey . '&cx=' . $googleClientId . '&q=' . $in . '&num=' . $resultCount . '';

            //                 $curl = curl_init();
            //                 curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            //                 curl_setopt($curl, CURLOPT_HEADER, false);
            //                 curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            //                 curl_setopt($curl, CURLOPT_URL, $url);
            //                 curl_setopt($curl, CURLOPT_REFERER, $url);
            //                 curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            //                 $html = curl_exec($curl);

            //                 curl_close($curl);
            //                 if ($html) {
            //                     $googleCSR = json_decode($html);
            //                     if (count($googleCSR) > 0) {
            //                         if($googleCSR->error){
            //                             $error = 'Your daily limit has been reached! Try again tomorrow';
            //                             return;
            //                         }
            //                         if(count($googleCSR->items)> 0){
            //                             foreach ($googleCSR->items as $items) {
            //                                 $arrResult[$j]['title'] = $items->title;
            //                                 $arrResult[$j]['link'] = $items->link;
            //                                 $arrResult[$j]['desc'] = $items->htmlSnippet;
            //                                 $j++;
            //                             }
            //                         }
            //                     }
            //                 } else {
            //                     $error = 'Your daily limit has been reached! Try again tomorrow';
            //                 }
            //             }
            //         }
            //     }
            // }

            // if ($isCustomSearch == 1) { // If result does not come from google custom search
            //     if (count($linkObjs) > 0) {
            //         foreach ($linkObjs as $linkObj) {
            //             $title = trim($linkObj->plaintext);
            //             $link = trim($linkObj->href);
            //                 // if it is not a direct link but url reference found inside it, then extract
            //                 if (!preg_match('/^https?/', $link) && preg_match('/q=(.+)&amp;sa=/U', $link, $matches) && preg_match('/^https?/', $matches[1])) {
            //                     $link = $matches[1];
            //                 } else if (!preg_match('/^https?/', $link)) { // skip if it is not a valid link
            //                     continue;
            //                 }

            //                 // description is not a child element of H3 therefore we use a counter and recheck.
            //                 switch ($typeDesc) {
            //                     case 'bing':
            //                         $descr = $html->find('div.b_caption p', $i);
            //                         break;
            //                     case 'google':
            //                     $descr = $html->find('span.st', $i);
            //                         break;
            //                     case 'hotbot':
            //                         $descr = $html->find('div.description', $i);
            //                         break;
            //                     case 'yahoo':
            //                         $descr = $html->find('div.compText p', $i);
            //                         break;
            //                 }
            //                 $i++;
            //                 $arrResult[$j]['title'] = $title;
            //                 $arrResult[$j]['link'] = $link;
            //                 $arrResult[$j]['desc'] = $descr;

            //                 $j++;
            //         }
            //     } else {
            //          $error = 'Your daily limit has been reached! Try again tomorrow';
            //     }
            // }
        } else {
            $error = 'Please enter a keyword';
        }
    }
    if (!empty($_POST['email']) && !empty($_POST['keywords'])) {
        $keywords = preg_split("/\r\n|\n|\r/", $_POST['keywords']);
        $html = null;
        if(!empty($keywords)){
            $html = json_encode($keywords);
        }
        $emails = $_POST['email'];
        $email = [
            $_POST['email']
        ];
        if(file_exists('emails.json')){
            $string = file_get_contents("emails.json",true);
            $fp = fopen("emails.json", "w");
            if (!empty($string)) {
                $email = json_decode($string);
                $email[] = $_POST['email'];
                $email = array_unique($email);
            }
            fwrite($fp, json_encode($email));
            fclose($fp);
        }else{
            $fp = fopen("emails.json","w");
            fwrite($fp,  json_encode($email));
            fclose($fp);
        }

        if (!empty($html)) {
            if (file_exists($_POST['email'] . '.json')) {
                $keywordString = file_get_contents($_POST['email'] . '.json', true);
                if (!empty($keywordString)) {
                    $keywordOld = json_decode($keywordString);

                    $keywords = array_merge($keywordOld,$keywords);


                    $keywords = array_unique(array_filter($keywords));

                    if (!empty($keywords)) {
                        $html = json_encode($keywords);
                    }
                }

            }
            $fp = fopen($_POST['email'] . '.json', 'w');
            fwrite($fp, $html);
            fclose($fp);
        }
    };
}

?>

<?php require_once 'header.php'; ?>

<style type="text/css">

  .content-wrapper {
    padding-top: 0px !important;
  }

  .main-footer {
    display: none;
  }

  .input-group .input-group-addon {
    color: #fff;
  }

  table {
    border: 1px solid #ccc;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    width: 100%;
    table-layout: fixed;
  }

  table caption {
    font-size: 1.5em;
    margin: .5em 0 .75em;
  }

  table tr {
    background-color: #f8f8f8;
    border: 1px solid #ddd;
    padding: .35em;
  }

  table th,
  table td {
    padding: .625em;
  }

  table th {
    font-size: .85em;
    letter-spacing: .1em;
    text-transform: uppercase;
  }

  @media screen and (max-width: 600px) {
    table {
      border: 0;
    }

    table caption {
      font-size: 1.3em;
    }

    table thead {
      border: none;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;
    }

    table tr {
      border-bottom: 3px solid #ddd;
      display: block;
      margin-bottom: .625em;
    }

    table td {
      border-bottom: 1px solid #ddd;
      display: block;
    }

    table td::before {
      /*
      * aria-label has no advantage, it won't be read inside a table
      content: attr(aria-label);
      */
      content: attr(data-label);
      float: left;
      font-weight: bold;
      text-transform: uppercase;
    }

    table td:last-child {
      border-bottom: 0;
    }
  }

</style>

<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-green layout-top-nav">
  <div class="wrapper">

    <!-- Full Width Column -->
    <div class="content-wrapper">

      <div style="width:100%" class="container">

        <!-- Main content -->
        <section class="content">

          <!------------------------------ /SEARCH TOOL ------------------------------>

          <form method="POST" id="f1" action="" class="form">
            <?php if (isset($error) && !empty($error)) { ?>
                <div class="row">
                  <div class="alert alert-danger">
                    <?php echo $error; ?>
                  </div>
                </div>
            <?php } ?>

            <div class="row">
              <div class="col-md-12">
                <h3 id="USE">Search now</h3>
                <div class="input-group">
                  <input name="keyword" id="tag" class="form-control input-lg" placeholder="Keyword" type="text">
                  <span class="input-group-btn">
                    <button id="btn_scraper" type="submit" class="btn btn-default btn-lg">Search!</button>
                  </span>
                </div><!-- /input-group -->

                <br>

                <div class="input-group">
                  <label>Record Per Page </label>
                  <select name="resultsize" class="form-control input-lg">
                    <option value="1" selected="">1</option>
                    <option value="5" >5</option>
                    <option value="10" selected="selected" >10</option>
                    <option value="20" >20</option>
                    <option value="30" >30</option>
                    <option value="40" >40</option>
                    <option value="50" >50</option>
                    <option value="100" >100</option>
                  </select>
                </div><!-- /input-group -->
                <br>
              </div>
              <input type='hidden' value='subbmit1' name='form'>
            </div>
          </form>
          <div class="row">
            <div class="col-md-12">
              <a href="#email" data-toggle="collapse" class="btn btn-success"><i class="fas fa-envelope"></i> Email Alerts?</a>

              <div id="email" class="collapse">

                <!---------- FORM ---------->

                <form method="post" id="form_submit" enctype="multipart/form-data" action="#">

                  <h4>Name</h4>
                  <div class="input-group">
                    <span id="suite" class="input-group-addon"><i class="fas fa-user"></i></span>
                    <input type="text" value="" class="form-control" name="name" id="contact_name" >
                  </div>

                  <h4>Email</h4>
                  <div class="input-group">
                    <span id="suite" class="input-group-addon"><i class="fas fa-at"></i></span>
                    <input type="text" value="" class="form-control" name="email" id="email">
                  </div>

                  <h4>Keywords</h4>
                  <div class="input-group">
                    <span id="suite" class="input-group-addon"><i class="fas fa-key"></i></span>
                    <textarea class="form-control"  name="keywords" placeholder="Enter keywords one line at a time" id="keywords"></textarea>
                  </div>

                  <div class="form-group">
                    <p><input type="Submit" name='form2' class="form-control btn btn-success" value="Submit"></p>
                  </div>

                </form>
                <!--Add new contact end-->

                <!---------- /FORM ---------->

              </div>

            </div>
          </div>
          <br/>

          <div class="row">
            <div class="col-md-12">
                <div class="col-md-12" style="text-align:right"><?= isset($searchEngine) ? 'Search Tool: '. $searchEngine : '' ?></div>
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th></th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (isset($arrResult) && count($arrResult) > 0) { ?>
                      <?php foreach ($arrResult as $key => $result) { ?>
                          <tr>
                            <td><img width="50px" src="images/reviews.png"></td>
                            <td><b><?php echo $result['title']; ?></b></td>
                            <td><?php echo $result['desc']; ?></td>
                            <td>
                              <a id="button_re<?php echo $key ?>" class="btn btn-default" style="display: none" onclick="$('#button_<?php echo $key ?>').show();$('#button_re<?php echo $key ?>').hide()">REPLIED</a>
                              <a id="button_<?php echo $key ?>" class="btn btn-default" onclick="PopupCenter(<?php echo $key ?>,$(this).attr('hrefz'), 'newwin', '1024', '600'); return false;" hrefz="<?php echo $result['link']; ?>">VIEW</a></td>
                          </tr>
                          <?php
                      }
                      ?>
                  <?php } else {
                      ?>
                      <tr>
                        <td colspan="4">No Result Found (search keyword to get results)</td>
                      </tr>
                      <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>


          <!------------------------------ /SEARCH TOOL ------------------------------>


        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->

    <!-- =================FOOTER====================== -->

    <!-- jQuery 3 -->
    <script src="https://suite.social/src/bower_components/jquery/dist/jquery.min.js"></script>

    <?php require_once 'footer-home.php'; ?>

    <script type="text/javascript">

                                                            /*document.addEventListener("contextmenu", function(e) {
                                                             "use strict";
                                                             e.preventDefault();
                                                             });*/

                                                            function PopupCenter(key,url, title, w, h) {
                                                                console.log(url);
                                                              // Fixes dual-screen position                         Most browsers      Firefox
                                                              var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                                                              var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                                                              var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                                                              var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                                                              var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                                                              var top = ((height / 2) - (h / 2)) + dualScreenTop;
                                                              var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

                                                              // Puts focus on the newWindow
                                                              if (window.focus) {
                                                                newWindow.focus();
                                                              }
                                                              $("#button_"+key).hide();
                                                              $("#button_re"+key).show();
                                                            }
                                                            <?php if(empty($_POST)):?>
                                                            function submitHastag(){
                                                                var tag = window.location.hash;
                                                                if(tag.length > 0){
                                                                $("#tag").val(tag.replace('#',''));
                                                                $("#f1").submit();
                                                            }
                                                            }
                                                            submitHastag();
                                                            <?php endif ?>
    </script>
  </div>
</body>
